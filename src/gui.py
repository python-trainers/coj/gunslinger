from trainerbase.gui import add_codeinjection_to_gui, simple_trainerbase_menu
from injections import no_reload, inf_hp, inf_concentration, max_xp


@simple_trainerbase_menu("Gunslinger", 400, 200)
def run_menu():
    add_codeinjection_to_gui(no_reload, "No Reload", "F1")
    add_codeinjection_to_gui(inf_hp, "Infinite HP", "F2")
    add_codeinjection_to_gui(inf_concentration, "Infinite Concentration", "F3")
    add_codeinjection_to_gui(max_xp, "Max XP")
