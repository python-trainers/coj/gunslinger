from trainerbase.codeinjection import CodeInjection, AllocatingCodeInjection
from memory import cojgunslinger


no_reload = CodeInjection(
    cojgunslinger.exe + 0x7F4DD9,
    """
        nop
        nop
        nop
    """,
)


max_xp = AllocatingCodeInjection(
    cojgunslinger.exe + 0x5B16A3,
    """
        mov edi, 1000000
        add [eax + 0x3C], edi
        mov ecx, [eax + 0x28]
    """,
    original_code_length=6,
)


inf_hp = AllocatingCodeInjection(
    cojgunslinger.exe + 0x5DFA0D,
    """
        fcomp dword [ebx + 0x8D4]
        mov dword [ebx + 0x8D4], 1000.0
    """,
    original_code_length=6,
)


inf_concentration = AllocatingCodeInjection(
    cojgunslinger.exe + 0x814094,
    """
        fst dword [edi + 0x8C]
        mov dword [ebx + 0x8C], 1.0
    """,
    original_code_length=6,
)
