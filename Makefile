run:
	pdm run python src/main.py

format:
	pdm run black src

lint: format
	pdm run pylint src
